package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        try {
            File file = new File("./src/com/company/PruebaEstructurasControl.txt");
            parser p = new parser(new Lexer(new FileReader(file)));
            Object result = p.parse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
