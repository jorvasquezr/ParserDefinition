
package com.company;

import java_cup.runtime.*;

%%

%class Lexer

%line
%column

%cup

letter = [a-zA-Z]
letterdigit = [a-zA-Z0-9]
pot = 8|16|24|32|40|48|56|64|72|80|88|96|104|112|120|128|136|144|152|160|168|176|184|192|200|208|216|224|232|240|248|256
byt = 1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32
payableKeyword = payable

MODIFIER = payable | internal
PRIVACY = private | public
ETYPE = address (" "{payableKeyword})? | bool | string | var | byte | (int|uint){pot}? | bytes{byt}? | (fixed|ufixed)([0-9]+x[0-9]+)?
IDENTIFIER = {letter} {letterdigit}*
BLANK = [ \t\r\n]+


zero = 0
decInt=[1-9][0-9]*
INTEGER=({zero}|{decInt})

Fractions1 = ([0] | [1-9][0-9]* )? + \. [0-9]*[1-9]
Fractions2 = [1-9][0-9]* + \.
Fraction = ({Fractions1}|{Fractions2})

NUMBER = ({INTEGER}|{Fraction})
Exponent=[e] [\-]? [1-9][0-9]*
SCIENTIFICNOTATION = [\-]? {NUMBER} {Exponent}

alfabetoHex= [/A/B/C/D/E/F0-9]+
HEXLITERAL = \h \e \x + ( \' {alfabetoHex} \' | \" {alfabetoHex} \")

BOOLEANLITERAL = (false|true)

StringCharacter = [^\r\n\"\'\\]
N = [a-fA-F0-9]
LineTerminator = \r|\n|\r\n

Code = ([0-9])+(\.([0-9])+)(\.([0-9])+)+
Comparator = (>|<)(=)?
VERSION = (\^|\~)? {Code} |{Comparator} {Code} (" "{Comparator} {Code})?

LINE_COMMENT = "//" [^\r\n]*
BLOCK_COMMENT_START = "/**"
COMMENT_CHARACTER = [^\r\n]
BLOCK_COMMENT_CLOSE = "*/"

%{
   public StringBuilder string = new StringBuilder();
    public boolean doubleQM;
    public boolean isError;
    public int hexcount = 0;

   private Symbol symbol(int type) {
       return new Symbol(type, yyline, yycolumn);
   }

   private Symbol symbol(int type, Object value) {
       //System.out.println("Token: " +((String) value)+ " Linea: " +yyline+ " Columna: " + yycolumn + " Value: " + type);
       return new Symbol(type, yyline, yycolumn, value);
   }
%}
%state STRING
%state COMMENT
%state COMMENT_LINE_START
%%

<YYINITIAL>
{
 "this"         { return symbol(sym.THIS, new String(yytext())); }
"if"            { return symbol(sym.IF, new String(yytext())); }
"else"          { return symbol(sym.ELSE, new String(yytext())); }
"for"           { return symbol(sym.FOR, new String(yytext())); }
"do"            { return symbol(sym.DO, new String(yytext())); }
"while"         { return symbol(sym.WHILE, new String(yytext())); }
"continue"      { return symbol(sym.CONTINUE, new String(yytext())); }
{LINE_COMMENT}  {/*ignore*/}
{BLOCK_COMMENT_START}   {yybegin(COMMENT);}

"!"              { return symbol(sym.NOT, new String(yytext())); }
"&&"             { return symbol(sym.AND, new String(yytext())); }
"||"             { return symbol(sym.OR, new String(yytext())); }
"=="             { return symbol(sym.EQUAL, new String(yytext())); }
"!="             { return symbol(sym.DIFF, new String(yytext())); }
">="             { return symbol(sym.LE, new String(yytext())); }
">"              { return symbol(sym.L, new String(yytext())); }
"<"              { return symbol(sym.B, new String(yytext())); }
"<="             { return symbol(sym.BE, new String(yytext())); }

"+="             { return symbol(sym.PLUSE, new String(yytext())); }
"-="             { return symbol(sym.MINUSE, new String(yytext())); }
"*="             { return symbol(sym.TIMESE, new String(yytext())); }
"/="             { return symbol(sym.DIVE, new String(yytext())); }
"+"             { return symbol(sym.PLUS, new String(yytext())); }
"-"             { return symbol(sym.MINUS, new String(yytext())); }

"?"             { return symbol(sym.QUESTION, new String(yytext())); }
":"             { return symbol(sym.COLON, new String(yytext())); }

"*"             { return symbol(sym.TIMES, new String(yytext())); }
"/"             { return symbol(sym.DIV, new String(yytext())); }
"%"             { return symbol(sym.MOD, new String(yytext())); }
"**"            { return symbol(sym.EXP, new String(yytext())); }

"\""      { yybegin(STRING); doubleQM = true; isError = false; string.setLength(0); string.append( '\"');  }
"\'"       { yybegin(STRING); doubleQM = false; isError = false; string.setLength(0); string.append( '\'');  }
"."             {return symbol(sym.DOT, new String(yytext())); }
{BOOLEANLITERAL}     { return symbol(sym.BOOLLIT,new String(yytext())); }
{SCIENTIFICNOTATION} { return symbol(sym.SCINOTLIT,new String(yytext())); }
{HEXLITERAL}         { return symbol(sym.HEXLIT,new String(yytext()));}
{NUMBER}               { return symbol(sym.NUMLIT,new String(yytext()));}
{VERSION}       { return symbol(sym.VERSION, new String(yytext())); }

"["             { return symbol(sym.LSB, new String(yytext())); }
"]"             { return symbol(sym.RSB, new String(yytext())); }
"{"             { return symbol(sym.LB, new String(yytext())); }
"}"             { return symbol(sym.RB, new String(yytext())); }
"("             { return symbol(sym.LP, new String(yytext())); }
")"             { return symbol(sym.RP, new String(yytext())); }
","             { return symbol(sym.COMMA, new String(yytext())); }
";"             { return symbol(sym.SEMI, new String(yytext())); }
"="             { return symbol(sym.EQ, new String(yytext())); }
"pragma"        { return symbol(sym.PRAGMA, new String(yytext())); }
"solidity"      { return symbol(sym.SOLIDITY, new String(yytext())); }
"contract"      { return symbol(sym.CONTRACT, new String(yytext())); }
 "struct"        { return symbol(sym.STRUCT, new String(yytext())); }
 "enum"          { return symbol(sym.ENUM, new String(yytext())); }
 "function"      { return symbol(sym.FUNC, new String(yytext())); }
 "return"       { return symbol(sym.RETURN, new String(yytext())); }
 "returns"       { return symbol(sym.RETURNS, new String(yytext())); }
 {MODIFIER}      { return symbol(sym.MODIFIER, new String(yytext())); }
 {PRIVACY}       { return symbol(sym.PRIV, new String(yytext())); }
 {ETYPE}         { return symbol(sym.ETYPE, new String(yytext())); }
 {IDENTIFIER}    { return symbol(sym.IDENT, new String(yytext())); }
 {BLANK}         { /**ignore**/ }
 <<EOF>>         { return symbol(sym.EOF); }
}

<STRING> {
 \'                             {if(!doubleQM){ yybegin(YYINITIAL);
                                      string.append( '\'' );
                                      if(!isError){
                                          return  symbol(sym.STRLIT, string.toString());
                                      }else{
                                        return symbol(sym.STRLIT, "Error");
                                      }
                                  }
                                }

 \"                           {if(doubleQM){ yybegin(YYINITIAL);
                                      string.append( '\"' );
                                      if(!isError){
                                          return  symbol(sym.STRLIT, string.toString());
                                      }else{
                                        return symbol(sym.STRLIT, "Error");
                                      }
                                  }
                                }
 {StringCharacter}*             {string.append( yytext() ); }

 /* escape sequences */
 "\\b"                          { string.append( yytext()); }
 "\\t"                          { string.append( yytext()); }
 "\\v"                          { string.append( yytext() ); }
 "\\n"                          { string.append( yytext() ); }
 "\\<newline>"                  { string.append( yytext() ); }
 "\\f"                          { string.append( yytext() ); }
 "\\r"                          { string.append( yytext() ); }
 "\\\""                         { string.append( yytext() ); }
 "\\'"                          { string.append( yytext() ); }
 "\\\\"                         { string.append( yytext() ); }
 "\\U" {N}{N}{N}{N}{N}{N}       { string.append( yytext());}
 "\\u" {N}{N}{N}{N}             { string.append( yytext());}
 "\\x" {N}{N}                   { string.append( yytext());}
 /* error cases */
 \\.                            { string.append( yytext()); isError = true; }
 {LineTerminator}               { yybegin(YYINITIAL);  System.err.println("Error weird literal:  "+ "(line: "+(yyline+1)+" , column: "+(yycolumn+1)+" , lexeme: '"+string.toString()+"')"); }
 <<EOF>>                        { yybegin(YYINITIAL);  System.err.println("Error weird literal:  "+ "(line: "+(yyline+1)+" , column: "+(yycolumn+1)+" , lexeme: '"+string.toString()+"')");  }
}

<COMMENT> {
    {BLOCK_COMMENT_CLOSE} {yybegin(YYINITIAL);}
    {LineTerminator}    {yybegin(COMMENT_LINE_START);}
    ({COMMENT_CHARACTER}|{BLANK}) {/*ignore*/}
}

<COMMENT_LINE_START> {
    {BLANK} {/*ignore*/}
    "*"                     { yybegin(COMMENT); }
    {BLOCK_COMMENT_CLOSE}   { yybegin(YYINITIAL); }
    .                       { yybegin(YYINITIAL); System.err.println("wrong line start for comment: "+ "(line: "+(yyline+1)+" , column: "+(yycolumn+1)+" , lexeme: '"+yytext()+"')");}
}

[^]                    { System.err.println("Weird lexeme: " +"(line: "+(yyline+1)+" , column: "+(yycolumn+1)+" , lexeme: '"+yytext()+"')"); }

